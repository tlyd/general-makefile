
#####################################################
#
# 模块通用Makefile
# 解决模块内部对外部依赖、模块的产出问题
# 支持多级文件，通用编译
# 编写人：wmh
# 编写时间：2021/08/05
#
#####################################################

# 1.输出产物配置
# 产物名称
MODULE_NAME         := entry
# 类型：exe(Executable program or application)、shared(Shared Object)、static(archive file)
TAG_TYPE            := exe
# 动态库版本，TAG_TYPE=shared时指定
LIB_VERSION         := 1.0.0
SO_VERSION          := 1
# 产物输出路径
TAG_PATH            := 

# 2.环境路径配置

# 模块源文件顶层路径
# 后面每个源文件路径（SRC_FILES）中的（MODULE_TOP_SRC_DIR）被tmp目录替换后就是.o、.d文件的存放路径
MODULE_TOP_SRC_DIR  := $(MODULE_PATH)/src



# 3.输出产物的依赖项，源文件，头文件，库文件

# 源文件路径path
MODULE_SRC_DIR      := $(MODULE_TOP_SRC_DIR) $(MODULE_TOP_SRC_DIR)/ext

# 头文件路径dir
MODULE_INC_DIR      := $(MODULE_PATH)/inc $(MODULE_PATH)/inc/ext

# 库文件与所在路径，file和dir
# 直接写库名，如libpthread.so，写pthread即可
LIBS                += 
MODULE_LIB_DIR      := $(MODULE_PATH)/lib/linux_64

# .o、.d文件存放顶层路径
MODULE_TMP_DIR      := $(MODULE_PATH)/tmp
# 模块输出文件路径
MODULE_OUT_DIR      := $(MODULE_PATH)/out