#include <stdio.h>

#include "sub.h"
#include "add.h"

#define LIB_TEST

#if 1

#undef LIB_TEST
#define LIB_TEST lib_test()

#include "cJSON.h"
int lib_test(void)
{
    cJSON *root = NULL;

    printf("%s\n", __func__);

    /* 创建一个JSON数据对象（Object）(链表头结点) 根节点*/
    root = cJSON_CreateObject();
    if (root == NULL)
    {
        printf("Create fail.\n");
        return -1;
    }
    /*值（value）可以是双引号括起来的字符串（string）、数值(number)、true、false、 null、对象（object）或者数组（array）。这些结构可以嵌套。查看代码发现，所有value都存储在对象里（是JSON数据），有释放问题
     */
    /* 根节点添加一条value为字符串类型，名称（name）为"name"的JSON数据(添加一个链表节点) */
    cJSON_AddStringToObject(root, "name", "APP");

    /* 根节点添加一条value为整数类型，名称（name）为"age"的JSON数据(添加一个链表节点)，
        value在函数里都自动转换为double类型
        CJSON_PUBLIC(cJSON*) cJSON_AddNumberToObject(cJSON * const object, const char * const name, const double number);
        */
    cJSON_AddNumberToObject(root, "age", 22);

    /* 根节点添加一条value为浮点类型，名称（name）为"weight"的JSON数据(添加一个链表节点) */
    cJSON_AddNumberToObject(root, "weight", 55.5);

    /* 添加一个值为 False 的布尔类型，名称（name）为"student"的JSON数据(添加一个链表节点) */
    cJSON_AddFalseToObject(root, "student");

    /* 打印JSON对象(整条链表)的所有数据 */
    char *str = NULL;
    str = cJSON_Print(root); /*这种方法安全吗？*/
    printf("%s\n", str);

    /*（重要）释放链表资源*/
    printf("free space\n");
    cJSON_Delete(root);

    /*把*/
    if (str != NULL)
        /* note: include '<stdlib.h>' or provide a declaration of 'free'，本示例就不加了，*/
        // free(str);
        /*cJSON.c里有调用<stdlib.h>*/
        cJSON_free(str);
    return 0;
}

#endif

int main(void)
{
    int a = 2, b = 3;

    printf("main\n");
    printf("a=%d b=%d\n", a, b);

    sub_int(a, b);
    add_int(a, b);

    LIB_TEST;
    return 0;
}
