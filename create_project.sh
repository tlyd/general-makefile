#	工程名
project="cJSON"
# 	工程目录路径
project_dir="."

mkdir ${project_dir}/${project} && cd ${project_dir}/${project}
mkdir src lib inc out tmp workshop

iftxt=0
if [ ${iftxt} -eq 1 ]; then
	{
		echo "src: 存放源文件(模块的，引用的)"
		echo "inc: 存放头文件(模块的，引用的)"
		echo "lib: 存放库(模块私用的)"
		echo "tmp: 存放编译中的过程文件"
		echo "out: 存放输出产物"
		echo "workshop: 存放输出产物"
	} >>readme.txt
fi
