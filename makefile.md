

# 预知



关于**运行**时，动态库的检索问题，此处提供两个办法：

1.   最简单的方法，可以把库放到可执行程序的同目录，演示执行是完全成功的。
2.   采用**临时追加环境变量**的方法，运行，验证Makefile功能正常

其他方法参考 [韦东山 Linux开发.md ](韦东山Linux开发/linux开发.md)第二章 应用开发基础

```shell
# Linux
export LD_LIBRARY_PATH={库路径}:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=/mnt/f/learn/General_module/makefile/general-makefile/test_code_single/lib/linux_64:$LD_LIBRARY_PATH

```



```powershell
#Windows

# CMD
set PATH=%PATH%;{库路径}

# PowerShell
$env:Path="{库路径};$env:Path"
$env:Path="F:\learn\General_module\makefile\general-makefile\test_code_single;$env:Path"
 
# Windows PowerShell 查看环境变量
env
ls env:
ls env:path
(type env:path) -split ';'

```



# 前期了解

![GCC](makefile.assets/gcc.PNG)

![这里写图片描述](makefile.assets/70.gif)

在成功预处理、编译、汇编后，就进入了链接阶段。这里涉及一个重要的概念：函数库。例如实现`printf`函数，在没有特别指定时，`gcc`会到Linux系统默认的搜索路径`/usr/lib`下进行查找，从而链接到`libc.so.6`函数库，这样就找到了`printf`函数实现。这就是链接的作用——组合已经编译好的代码。

函数库一般分为静态库和动态库。

静态库：是指在编译链接时，把库文件的代码全部加入到可执行文件中，因此生成的文件较大，但在运行时也就不再需要这些静态库文件。

动态库：在编译链接时并没有把库文件的代码加入到可执行文件中，而是在程序执行时由链接文件加载库，这样可以节省系统的开销，**链接时仍需指定**。



| 平台    | 动态库文件后缀               | 静态库文件后缀         |
| ------- | ---------------------------- | ---------------------- |
| Linux   | `.so`(Shared Object)         | `.a`(archive file)     |
| Windows | `.dll`(Dynamic Link Library) | `.lib`(Static Library) |



实际当中程序文件比较多，这时候对文件进行分类，分为头文件（.h）、源文件（.c/.cpp）、目标文件（.o）、输出产物（库，可执行程序）。将文件按照文件类型放在不同的目录当中，方便管理（调用、清空、输出）

\-----------------------------------------------------
**GCC文件后缀名：**

　　.c为后缀的文件，C语言源代码文件；
　　.a为后缀的文件，是由目标文件构成的档案库文件；
　　.C，.cc或.cxx 为后缀的文件，是C++源代码文件；
　　.h为后缀的文件，是程序所包含的头文件；
　　.i 为后缀的文件，是已经预处理过的C源代码文件；
　　.ii为后缀的文件，是已经预处理过的C++源代码文件；
　　.m为后缀的文件，是Objective-C源代码文件；
　　.o为后缀的文件，是编译后的目标文件；
　　.s为后缀的文件，是汇编语言源代码文件；
　　.S为后缀的文件，是经过预编译的汇编语言源代码文件。
\-----------------------------------------------------

脚本 `create_project.sh`方便给一个新的工程创建这些目录

我的工程构想：

1.   工程包含许多模块，模块的输出产物可以是库、可执行程序
2.   鉴于模块之间依赖关系，输出产物为库则只生成动态库。
3.   本文的Makefile只为`模块级Makefile`，最深度也是将输出产物复制到配置的文件夹。
4.   本文的Makefile不构成**编译系统**（工程级的编译工作，可能还有打包工作，涉及多进程，依赖处理等）



通用模块Makefile编写参考：[参考](https://www.cnblogs.com/Anker/p/3242207.html)，[参考](https://blog.csdn.net/yuzhihui_no1/article/details/44810357)，[参考](https://blog.csdn.net/guoxiaojie_415/article/details/52206139)

```makefile
DIR_INC = ./inc
DIR_SRC = ./src
DIR_OBJ = ./obj
DIR_BIN = ./bin

SRC = $(wildcard ${DIR_SRC}/*.c)
OBJ = $(patsubst %.c,${DIR_OBJ}/%.o,$(notdir ${SRC}))

TARGET = main

BIN_TARGET = ${DIR_BIN}/${TARGET}

CC = gcc
CFLAGS = -g -Wall -I${DIR_INC}

${BIN_TARGET}:${OBJ}
    $(CC) $(OBJ)  -o $@

${DIR_OBJ}/%.o:${DIR_SRC}/%.c
    $(CC) $(CFLAGS) -c  $< -o $@
.PHONY:clean
clean:
    find ${DIR_OBJ} -name *.o -exec rm -rf {}
    rm -rf $(BIN_TARGET)
```

# Makefile中预定义变量表

`$@` 目标的完整名称。
`$*` 不包含扩展名的目标文件名称。
`$<` 第一个依赖文件的名称。
`$+` 所有的依赖文件，以空格分开，并以出现的先后为序，可能包含重复的依赖文件。
`$^` 所有的依赖文件，以空格分开，不包含重复的依赖文件。
`$?` 所有的依赖文件，以空格分开，这些依赖文件的修改日期比目标的创建日期晚。
`$%` 如果目标是归档成员，则该变量表示目标的归档成员名称。例如，如果目标名称为 mytarget.so(image.o)，则 @ 为 mytarget.so，% 为 image.o。

`AR：		归档维护程序的名称，默认值为 ar。`
`ARFLAGS：	 归档维护程序的选项。`
`AS：		汇编程序的名称，默认值为 as。`
`ASFLAGS：	 汇编程序的选项。`
`CC：			C 编译器的名称，默认值为 cc。`
`CCFLAGS：	C 编译器的选项。`
`CPP：			 C 预编译器的名称，默认值为 $(CC) -E。`
`CPPFLAGS： C 预编译的选项。`
`CXX： 			C++ 编译器的名称，默认值为 g++。`
`CXXFLAGS： C++ 编译器的选项。`
`FC FORTRAN： 编译器的名称，默认值为 f77。`
`FFLAGS：		FORTRAN 编译器的选项。`

# 基本字符串处理函数

makefile使用函数：$(函数名+空格+函数参数)

​    wildcard 这是扩展通配符函数，功能是展开成一列所有符合由其参数描述的文件名，文件间以空格间隔；比如：罗列出src下的所有.c文件：$(wildcard src/*.c)，可用于指定编译当前目录下所有.c文件。如果还有子目录，比如子目录为inc，则再增加一个`wildcard`函数：`SRC = $(wildcard *.c) $(wildcard inc/*.c)`

​    patsubst 这是匹配替换函数， patsubst（ 需要匹配的文件样式，匹配替换成什么文件，需要匹配的源文件）函数。比如：用src下的*.c替换成对应的 *.o文件存放到obj中：$(patsubst  %.c, ${OBJ}/%.o, $(notdir $(SOURCE)))

​    notdir 这是去除路径函数，去除SOURCE中文件的所有目录，只留下文件名，例如/tmp/test.c，去除路径后为test.c；



`-I -L -lword`的区别：

​    `gcc -o hello hello.c -I /home/hello/include -L /home/hello/lib -lworld`

​    上面这句表示在编译`hello.c`时`-I /home/hello/include`表示将`/home/hello/include`目录作为第一个寻找头文件的目录，寻找的顺序是：`/home/hello/include-->/usr/include-->/usr/local/include`

 `-L /home/hello/lib`表示将`/home/hello/lib`目录作为第一个寻找库文件的目录，寻找的顺序是：`/home/hello/lib-->/lib-->/usr/lib-->/usr/local/lib`

`-lworld`表示在上面的lib的路径中寻找`libworld.so`动态库文件（如果`gcc`编译选项中加入了`“-static”`表示寻找`libworld.a`静态库文件）



二、Make程序中有许多选项，其中最常用的3个选项为：

1、-k：作用是在让make命令在发现错误时仍然就执行，而不是在检测到第一个错误时就停止，所以可是使用这个选项在一次操作中发下所有未编译成功的源文件

2、-n：作用是让make命令输出将要执行的操作步骤，而不是真正执行这些操作

3、`-f <filename>`：作用是告诉make将文件名为filename的我文件作为makefile文件。如果未使用这个选项，标准版的make命令将优先在当前命令下查找名称为makefile的文件，如果不存在名称makefile的文件，则开始查找名为Makefile的文件。

# 通用makefile

## 第一种，单独Makefile

[参考](https://segmentfault.com/a/1190000003756084)，[参考](https://blog.csdn.net/wooin/article/details/580812)

```makefile
######################################
# Copyright (c) 1997 George Foot (george.foot@merton.ox.ac.uk)
# All rights reserved.
######################################
#目标（可执行文档）名称，库（譬如stdcx,iostr,mysql等），头文件路径
DESTINATION := test
LIBS := 
INCLUDES := .


RM := rm -f
#C,CC或CPP文件的后缀
PS=cpp
# GNU Make的隐含变量定义
CC=g++
CPPFLAGS = -g -Wall -O3 -march=i486
CPPFLAGS += $(addprefix -I,$(INCLUDES))
CPPFLAGS += -MMD

#以下部分无需修改
SOURCE := $(wildcard *.$(PS))
OBJS := $(patsubst %.$(PS),%.o,$(SOURCE))
DEPS := $(patsubst %.o,%.d,$(OBJS))
MISSING_DEPS := $(filter-out $(wildcard $(DEPS)),$(DEPS))
MISSING_DEPS_SOURCES := $(wildcard $(patsubst %.d,%.$(PS),$(MISSING_DEPS)))

.PHONY : all deps objs clean rebuild

all : $(DESTINATION)

deps : $(DEPS)
         $(CC) -MM -MMD $(SOURCE)

objs : $(OBJS)

clean :
         @$(RM) *.o
         @$(RM) *.d
         @$(RM) $(DESTINATION)

rebuild: clean all 

ifneq ($(MISSING_DEPS),)
$(MISSING_DEPS) :
         @$(RM) $(patsubst %.d,%.o,$@)
endif

-include $(DEPS)

$(DESTINATION) : $(OBJS)
         $(CC) -o $(DESTINATION) $(OBJS) $(addprefix -l,$(LIBS))
#结束
```



- 简介（通过研究这个Makefile 文件可以很好的理解Makefile的规则）：
    - 原作者是Gorge Foot，写这个Makefile的时候还是一个学生
    - ":="赋值，和"="不同的是，":="在赋值的同时，会将赋值语句中所有的变量就地展开，也就是说，A:=$(B)后，B的值的改变不再影响A
    - `隐含规则`。GUN Make在不特别指定的情况下会使用诸如以下编译命令：$(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c $< -o $@，这也是该Makefile最后一个命令没有添加$(CPPFLAGS)的原因，因为缺省是包含这个变量的
    - `函数`和变量很相似："$ (函数名，空格，一列由逗号分隔的参数)"
    - SOURCES = $(wildcard *.cpp) 列出工作目录下文件名满足"*.cpp"条件的文件，以空格分隔，并将列表赋给SOURCE变量
    - `patsubst`函数：3个参数。功能是将第三个参数中的每一项（由空格分隔）符合第一个参数描述的部分替换成第二个参数制定的值
    - `addprefix`函数：2个参数。将源串（第2个参数，由空格分隔）中的每一项添加前缀（第1个参数）
    - `filter-out`函数：2个参数。从第二串中过滤掉包含在第一个串中的项
    - $(CC) -MM -MMD $(SOURCE) : 对每个源文件生成依赖(dependence，Make通过依赖规则来判断是否需要重新编译某个文件)，"D"生成".d"文件，-MM表示去掉 depends里面的系统的头文件(使用<>包含的头文件)（若使用-M则全部包含，事实上，系统头文件被修改的可能性极小，不需要执行依赖检查）
    - `.PHONY`，不检查后面指定各项是否存在同名文件
    - ifneg...else...endif，Makefile中的条件语句
    - -include $(DEPS) : 将DEPS中的文件包含进来，"-"表示忽略文件不存在的错误
    - @$(RM) *.o : 开头的"@"表示在Make的时候，不显示这条命令（GNU Make缺省是显示的)
    - all : 作为第一个出现的目标项目，Make会将它作为主要和缺省项目("make"就表示"make all")
    - deps : 只生成依赖文件(.d文件)
    - objs : 为每一个源码程序生成或更新 '.d' 文件和'.o'文件
    - clean : 删除所有'.d','.o'和可执行文件
    - rebuild : clean然后重建
    - 内部变量$@, $< $^ : 分别表示目标名(:前面的部分，比如all)，依靠列表（:后面的部分）中的第一个依靠文件，所有依靠文件

编译前：

<img src="makefile.assets/image-20210623153424704.png" alt="image-20210623153424704" style="zoom:67%;" />

编译后

<img src="makefile.assets/image-20210623154016752.png" alt="image-20210623154016752" style="zoom:67%;" />

优点：支持多个目录的源文件、头文件、库文件（这个库文件没试）

不足：中间文件生成在相应源文件目录，并未划分到obj和dep文件夹。没有目录存在判定



### 1 动态库编译问题

#### 共享库命名约定

1. **realname** ：共享库本身的文件名

    lib + 库名 + .so.主版本号.小版本号.build号

    这个文件是含有实际的可执行的二进制代码的，大小明显大于另外两个文件。

2. **soname**（ Short for shared object name）：应用程序运行时，寻找共享库用的文件名

    lib + 库名 + .so.主版本号

    由`realname`的共享库产生，软链接

3) **linkname** ：链接时，寻找的共享库的文件名。如链接时指定`-lcjson`参数，链接器就在库目录寻找`libcjson.so`，而不是另外两个文件，如果没有就会报错找不到库。

    lib + 库名 + .so
    
    但是使用脚本，升级等情况下很方便，由`soname`或`realname`的共享库产生



看一份开源工程实例，[cJSON](https://github.com/DaveGamble/cJSON)，此时版本为cJSON-1.7.14

下载、[解压、]编译、安装（复制）

```shell
git clone https://github.com/DaveGamble/cJSON.git
cd cJSON
# 或者下载压缩包
tar xzvf cJSON-1.7.14.tar.gz
cd cJSON-1.7.14

make all
make PREFIX=/ DESTDIR=./install/cJSON-1.7.14 install
cd ./install/cJSON-1.7.14/lib
```

![image-20210803135201912](makefile.assets/image-20210803135201912.png)

看到`cJSON-1.7.14`代码制作成动态库后

- `realname`为`libcjson.so.1.7.14`
- `soname`为`libcjson.so.1`
- `linkname`为`libcjson.so`

注意：

-   在linux里`soname`和`linkname`是`realname`软链接，正常大小不一样；
-   Linux中，`cp`软链接文件时默认复制指向的实体，若想复制软链接文件本身，需加`-d`选项。
-   Windows系统不支持软链接，显示软链接大小为0，Windows复制后大小和实体大小差不多；

查看库的信息，对可执行程序同样可用

```shell
file libcjson.so.1.7.14

readelf -d libcjson.so.1.7.14
# 可以只看文件（库、可执行程序...）的依赖库
readelf -d libcjson.so.1.7.14 | grep NEEDED

# 打印依赖库
ldd libcjson.so.1.7.14 

# 文件格式，包含的函数
objdump -tT liblistdevs.so > listdevs.txt
nm liblistdevs.so
```



```shell
➜  file libcjson.so.1.7.14
libcjson.so: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=c75029665f07a5e963565dc0af797f0301f6dc90, not stripped
# 64位Linux动态库

➜  file .\libcjson.so.1.7.14
.\libcjson.so.1.7.14: PE32+ executable (DLL) (console) x86-64, for MS Windows
# 64位Windows动态库
```



<img src="makefile.assets/image-20210803140524644.png" alt="image-20210803140524644" style="zoom: 67%;" />

可以看到 `cjson`库文件包含 `soname`文件名，所以链接时的`linkname`的库，已经把运行时库的`soname`文件名告知给了可自行程序或库



```shell
➜  ldd libcjson.so.1.7.14
        linux-vdso.so.1 (0x00007fffc5583000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f4711c60000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f4711e7b000)
# 依赖库为64位

 ldd .\libcjson.dll
        ntdll.dll => /cygdrive/c/Windows/SYSTEM32/ntdll.dll (0x7ffdeaa30000)
        KERNEL32.DLL => /cygdrive/c/Windows/System32/KERNEL32.DLL (0x7ffde97c0000)
        KERNELBASE.dll => /cygdrive/c/Windows/System32/KERNELBASE.dll (0x7ffde8400000)
        apphelp.dll => /cygdrive/c/Windows/SYSTEM32/apphelp.dll (0x7ffde55f0000)
        cygwin1.dll => /usr/bin/cygwin1.dll (0x180040000)
        advapi32.dll => /cygdrive/c/Windows/System32/advapi32.dll (0x7ffdea940000)
        msvcrt.dll => /cygdrive/c/Windows/System32/msvcrt.dll (0x7ffde9970000)
        sechost.dll => /cygdrive/c/Windows/System32/sechost.dll (0x7ffdea2b0000)
        RPCRT4.dll => /cygdrive/c/Windows/System32/RPCRT4.dll (0x7ffdea420000)
        CRYPTBASE.DLL => /cygdrive/c/Windows/SYSTEM32/CRYPTBASE.DLL (0x7ffde7a20000)
        bcryptPrimitives.dll => /cygdrive/c/Windows/System32/bcryptPrimitives.dll (0x7ffde8840000)
# # 依赖库为64位 
```



```shell
➜  objdump -tT libcjson.so

./lib/libcjson.so：     文件格式 elf64-x86-64

SYMBOL TABLE:
00000000000002a8 l    d  .note.gnu.property     0000000000000000              .note.gnu.property
00000000000002c8 l    d  .note.gnu.build-id     0000000000000000              .note.gnu.build-id
00000000000002f0 l    d  .gnu.hash      0000000000000000              .gnu.hash
0000000000000588 l    d  .dynsym        0000000000000000              .dynsym
...
0000000000000000  w    F *UND*  0000000000000000              __cxa_finalize@@GLIBC_2.2.5
00000000000065d8 g     F .text  000000000000004c              cJSON_CreateStringReference
0000000000002539 g     F .text  000000000000001b              cJSON_GetErrorPtr


DYNAMIC SYMBOL TABLE:
0000000000000000      DF *UND*  0000000000000000  GLIBC_2.2.5 free
0000000000000000      DF *UND*  0000000000000000  GLIBC_2.2.5 strncmp
0000000000000000  w   D  *UND*  0000000000000000              _ITM_deregisterTMCloneTable
0000000000000000      DF *UND*  0000000000000000  GLIBC_2.2.5 strcpy
0000000000000000      DF *UND*  0000000000000000  GLIBC_2.2.5 strtod
0000000000000000      DF *UND*  0000000000000000  GLIBC_2.2.5 strlen
...
0000000000005e7f g    DF .text  00000000000000c3  Base        cJSON_DetachItemViaPointer
0000000000002585 g    DF .text  0000000000000035  Base        cJSON_GetNumberValue
0000000000006491 g    DF .text  0000000000000045  Base        cJSON_CreateBool
000000000000714d g    DF .text  000000000000002e  Base        cJSON_IsBool
0000000000006282 g    DF .text  0000000000000051  Base        cJSON_ReplaceItemInArray

```



这库和库名是怎么实现的呢？

摘取`cJSON`的`Makefile`里有关动态库的代码，里面行末的"//"是我写的注释，方便理解

![image-20210803142509905](makefile.assets/image-20210803142509905.png)



编译时，库的链接有两种形式：

2. 使用"`-l库名`"，如`-lcjson`，并指定库的路径 `-Llib_path`

    因为链接器会根据库名，合成库文件的`linkname`，并查找该库文件，所以库指定路径要存在文件名为`linkname`的库文件。而运行时又需要文件名为`soname`的库。为避免复制两次，占用空间，应该上面那样，创建两个软链接。
    
    （工程建议：输出产物若为库，就**必须**创建相应的`linkname`、`soname`的软链接，再把三个这样输出产物复制（`cp -d`）到相应的**运行时**目录，这样链接，运行在类似目录，方便打包工作）
    
2. 直接给出路径+库文件名，如`./lib/libcjson.so.1`

    因为路径已经给出了，链接程序能找到这个库，这不需要指定路径（`-L`）和库名（`-l`）。指定库路径的目的也是让链接程序能找到这个库。这种是仅用作编译运行是很方便的，只需把`realname`的库重命名为`soname`，链接和运行，这样一个库文件就满足。没有软链接，但是升级移植库时较麻烦。**故舍弃，不推荐**。



```makefile
# 链接动态库
LDFLAGS             += -lws2_32
MODULE_LIB_DIR      := C:/Windows/System32/
LDFLAGS += $(addprefix -L,$(MODULE_LIB_DIR))
```

上面形式1没找到合适的通配符，待以后解决

注意：复制安装时，cp时要加-d选项，避免再复制一遍本体

```shell
# libcjson.so —> libcjson.so.1
ln -sf libcjson.so.1 libcjson.so
```



### 2 关于 `-Wl,-rpath/-rpath-link`

关于`-Wl，`，[参考](http://www.360doc.com/content/12/0211/08/6828497_185707599.shtml)

-rpath/-rpath- link其实都是ld的option，不是gcc的。gcc只是一个wrapper，将preprocessor, assemble, link三者结合了起来。要在gcc的命令行中直接使用这两个option（-rpath、-rpath-link），必须遵循语法：-Wl,......。比 如：-Wl,--rpath-link /opt/alp/lib。

-Wl就是告诉gcc后面的内容是传递给linker的option。如果直接使用ld的话，就不需要-Wl,了。

这里是gcc命令行中给assembler, preprocessor, linker传递option的具体关键字列表：

- ` -Wa,<options>      Pass comma-separated <options> on to the assembler ` 
- `-Wp,<options>      Pass comma-separated <options> on to the preprocessor `
- `-Wl,<options>      Pass comma-separated <options> on to the linker`





两者和`-L`差别[参考](https://my.oschina.net/u/4393789/blog/3790060)

`-rpath <dir>`的作用是手动将一个目录强行指定成一个.so文件的搜索目录，他的优先级在LD_LIBRARY_PATH和/etc /ld.so.conf...这些地方定义的.so文件搜索目录之上。由于这个目录是由程序的开发人员在编译的时候指定的，所以将来这个程序到了其他机器 上运行的时候，这个目录是不能修改的。-rpath指定的路径会被记录在生成的可执行程序中，用于运行时查找需要加载的动态库。-rpath-link 则只用于链接时查找。

- -L:： **链接**的时候，找库的目录会先从 -L 指定的目录去找，然后是默认的地方。这个是
    编译时的-L选项并不影响环境变量LD_LIBRARY_PATH，-L只是指定了程序链接时库的路径，并不影响程序执行时库的路径，系统还是会到默认路径下查找该程序所需要的库，如果找不到，还是会报错，类似cannot open shared object file。
- -rpath-link： 这个也是**链接**时候用的，库也是需要依赖库的，这个选项指定**库**所需库的目录。例如你显式地指定需要 liba.so，但是 liba.so本身是依赖 libb.so，libb.so在默认路径里查找不到，此时需要指定libb.so路径。 liba.so 引用到 libb.so时，会先从 -rpath-link 给的目录里找。

- -rpath：**链接**时指定程序**运行**的时候查找动态库的目录。**运行**的时候，要找 .so 文件，会从先这个选项里指定的地方去找。

Makefile-clean_edition版本给出了把对象文件（.o）、依赖文件（.d）生成到其他目录的方法。

也参考了韦东山的删除，和编译前缀写法


--sysroot=dir 的作用

如果在编译时指定了-sysroot=dir 就是为编译时指定了逻辑目录。编译过程中需要引用的库，头文件，如果要到/usr/include目录下去找的情况下，则会在前面加上逻辑目录。

### 3  位置无关

如果想创建一个动态链接库，可以使用 GCC 的`-shared`选项。输入文件可以是源文件、汇编文件或者目标文件。

另外还得结合`-fPIC`选项。-fPIC 选项作用于**编译**阶段，告诉编译器产生与位置无关代码（Position-Independent Code）；这样一来，产生的代码中就没有绝对地址了，全部使用相对地址，所以代码可以被加载器加载到内存的任意位置，都可以正确的执行。这正是共享库所要求的，共享库被加载时，在内存的位置不是固定的。



## 另一种韦东山版的，多级Makefile

[参考](https://blog.csdn.net/weixin_42832472/article/details/110144332)

顶层Makefile，一般就一个

```makefile
CROSS_COMPILE = 
AS		= $(CROSS_COMPILE)as
LD		= $(CROSS_COMPILE)ld
CC		= $(CROSS_COMPILE)gcc
CPP		= $(CC) -E
AR		= $(CROSS_COMPILE)ar
NM		= $(CROSS_COMPILE)nm

STRIP		= $(CROSS_COMPILE)strip
OBJCOPY		= $(CROSS_COMPILE)objcopy
OBJDUMP		= $(CROSS_COMPILE)objdump

export AS LD CC CPP AR NM
export STRIP OBJCOPY OBJDUMP

CFLAGS := -Wall -O2 -g
CFLAGS += -I $(shell pwd)/inc/own
CFLAGS += -I $(shell pwd)/inc/ext

LDFLAGS := 

export CFLAGS LDFLAGS

TOPDIR := $(shell pwd)
export TOPDIR

TARGET := ./out/app


obj-y += 
obj-y += src/


all : start_recursive_build $(TARGET)
	@echo $(TARGET) has been built!

start_recursive_build:
	make -C ./ -f $(TOPDIR)/Makefile.build

$(TARGET) : built-in.o
	$(CC) -o $(TARGET) built-in.o $(LDFLAGS)

clean:
	rm -f $(shell find -name "*.o")
	rm -f $(TARGET)

distclean:
	rm -f $(shell find -name "*.o")
	rm -f $(shell find -name "*.d")
	rm -f $(TARGET)

```

顶层`Makefile.build`，一般就一个

```makefile
PHONY := __build
__build:


obj-y :=
subdir-y :=
EXTRA_CFLAGS :=

include Makefile

# obj-y := a.o b.o c/ d/
# $(filter %/, $(obj-y))   : c/ d/
# __subdir-y  : c d
# subdir-y    : c d
__subdir-y	:= $(patsubst %/,%,$(filter %/, $(obj-y)))
subdir-y	+= $(__subdir-y)

# c/built-in.o d/built-in.o
subdir_objs := $(foreach f,$(subdir-y),$(f)/built-in.o)

# a.o b.o
cur_objs := $(filter-out %/, $(obj-y))
dep_files := $(foreach f,$(cur_objs),.$(f).d)
dep_files := $(wildcard $(dep_files))

ifneq ($(dep_files),)
  include $(dep_files)
endif


PHONY += $(subdir-y)


__build : $(subdir-y) built-in.o

$(subdir-y):
	make -C $@ -f $(TOPDIR)/Makefile.build

built-in.o : $(cur_objs) $(subdir_objs)
	$(LD) -r -o $@ $^

dep_file = .$@.d

%.o : %.c
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) $(CFLAGS_$@) -Wp,-MD,$(dep_file) -c -o $@ $<
	
.PHONY : $(PHONY)

```

子Makefile，一般有多个，有源文件的地方都要有子Makefile，到顶层Makefile**路径上**也要有子Makefile，作相应修改，指明当前目录源文件和下级有源文件的目录。

```makefile
EXTRA_CFLAGS  := 
CFLAGS_file.o := 

obj-y += 
obj-y += own/
obj-y += ext/
#    "obj-y += file.o"  表示把当前目录下的file.c编进程序里，
#    "obj-y += subdir/" 表示要进入subdir这个子目录下去寻找文件来编进程序里，是哪些文件由subdir目录下的Makefile决定。
#    "EXTRA_CFLAGS",    它给当前目录下的所有文件(不含其下的子目录)设置额外的编译选项, 可以不设置
#    "CFLAGS_xxx.o",    它给当前目录下的xxx.c设置它自己的编译选项, 可以不设置

```



使用

```
本程序的Makefile分为3类:
1. 顶层目录的Makefile
2. 顶层目录的Makefile.build
3. 各级子目录的Makefile

一、各级子目录的Makefile：
   它最简单，形式如下：

EXTRA_CFLAGS  := 
CFLAGS_file.o := 

obj-y += file.o
obj-y += subdir/
   
   "obj-y += file.o"  表示把当前目录下的file.c编进程序里，
   "obj-y += subdir/" 表示要进入subdir这个子目录下去寻找文件来编进程序里，是哪些文件由subdir目录下的Makefile决定。
   "EXTRA_CFLAGS",    它给当前目录下的所有文件(不含其下的子目录)设置额外的编译选项, 可以不设置
   "CFLAGS_xxx.o",    它给当前目录下的xxx.c设置它自己的编译选项, 可以不设置

注意: 
1. "subdir/"中的斜杠"/"不可省略
2. 顶层Makefile中的CFLAGS在编译任意一个.c文件时都会使用
3. CFLAGS  EXTRA_CFLAGS  CFLAGS_xxx.o 三者组成xxx.c的编译选项

二、顶层目录的Makefile：
   它除了定义obj-y来指定根目录下要编进程序去的文件、子目录外，
   主要是定义工具链前缀CROSS_COMPILE,
   定义编译参数CFLAGS,
   定义链接参数LDFLAGS,
   这些参数就是文件中用export导出的各变量。

三、顶层目录的Makefile.build：
   这是最复杂的部分，它的功能就是把某个目录及它的所有子目录中、需要编进程序去的文件都编译出来，打包为built-in.o
   详细的讲解请看视频。

四、怎么使用这套Makefile：
1．把顶层Makefile, Makefile.build放入程序的顶层目录
   在各自子目录创建一个空白的Makefile

2．确定编译哪些源文件
   修改顶层目录和各自子目录Makefile的obj-y : 
    obj-y += xxx.o
	obj-y += yyy/
	这表示要编译当前目录下的xxx.c, 要编译当前目录下的yyy子目录	

3. 确定编译选项、链接选项
   修改顶层目录Makefile的CFLAGS，这是编译所有.c文件时都要用的编译选项;
   修改顶层目录Makefile的LDFLAGS，这是链接最后的应用程序时的链接选项;
   
   修改各自子目录下的Makefile：
   "EXTRA_CFLAGS",    它给当前目录下的所有文件(不含其下的子目录)设置额外的编译选项, 可以不设置
   "CFLAGS_xxx.o",    它给当前目录下的xxx.c设置它自己的编译选项, 可以不设置
   
4. 使用哪个编译器？
   修改顶层目录Makefile的CROSS_COMPILE, 用来指定工具链的前缀(比如arm-linux-)
   
5. 确定应用程序的名字：
   修改顶层目录Makefile的TARGET, 这是用来指定编译出来的程序的名字

6. 执行"make"来编译，执行"make clean"来清除，执行"make distclean"来彻底清除

```

编译前后

<img src="makefile.assets/image-20210623164046337.png" alt="image-20210623164046337" style="zoom: 60%;" /><img src="makefile.assets/image-20210623164202644.png" alt="image-20210623164202644" style="zoom:60%;" />



需求增加：支持多模块，多进程，多动态库编译

有些代码集合作为一个模块，

1.   目标生成一个对象文件（.o），没有main函数，像一个静态库一样等待被包含
2.   目标生成一个可执行，有main函数，作为一个独立进程使用
3.   目标生成一个动态库（.so）
